import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { LoginModel } from './models/login-model';
import { UserModel } from './models/user-model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public readonly UserKey: string = 'user';

  constructor() { }

  public login(loginModel: LoginModel): Observable<UserModel> {

    // Simulate calling an API for loggging in by just returning a mock user.
    const user: UserModel = {
      id: 1,
      username: loginModel.username,
      firstName: 'Test',
      lastName: 'User'
    };

    // Store user information in local storage.
    localStorage.setItem(this.UserKey, JSON.stringify(user));

    return of(user).pipe(delay(1500));
  }

  public logout(): void {
    // If the user is in localstorage, remove them.
    const user = this.getUser();

    if (user) {
      localStorage.removeItem(this.UserKey);
    }
  }

  public isAuthenticated(): boolean {
    const user = this.getUser();

    if (user) {
      return true;
    }

    return false;
  }

  public getUser(): UserModel {
    const user = localStorage.getItem(this.UserKey);

    if (user) {
      return JSON.parse(user);
    }

    return null;
  }
}
