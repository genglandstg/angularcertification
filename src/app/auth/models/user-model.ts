export interface UserModel {
    id: number,
    username: string,
    firstName: string,
    lastName: string
}