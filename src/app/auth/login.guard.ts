import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class LoginGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) {}

    canActivate(): boolean {

        // We're landing on the login page. If we're already authenticated,
        // navigate to the dashboard. Otherwise, stay on the page.
        if (this.authService.isAuthenticated()) {
            this.router.navigate(['/dashboard']);
            return false;
        }

        return true;
    }
}