import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { LoginModel } from '../models/login-model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public showPassword: boolean = false;
  public disableLoginButton: boolean = false;

  public loginForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  });

  constructor(private authService: AuthService, private router: Router) { }

  public ngOnInit(): void {
  }

  public onLogin(): void {

    this.disableLoginButton = true;

    const loginModel: LoginModel = {
      username: this.loginForm.value.username,
      password: this.loginForm.value.password
    };

    this.authService.login(loginModel).subscribe(userModel => {
      this.disableLoginButton = false;

      // Log in success. Navigate to the dashboard.
      this.router.navigate(['dashboard']);
    });
  }

  public passwordShowHide(): void {
    this.showPassword = !this.showPassword;
  }

}
