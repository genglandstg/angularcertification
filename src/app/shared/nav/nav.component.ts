import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { UserModel } from 'src/app/auth/models/user-model';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public loggedInUser: UserModel;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {

    // Determine if a user is logged in. This route is guarded, so they should be.
    this.loggedInUser = this.authService.getUser();
  }

  public logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
